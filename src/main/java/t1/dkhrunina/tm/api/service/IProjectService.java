package t1.dkhrunina.tm.api.service;

import t1.dkhrunina.tm.api.repository.IProjectRepository;
import t1.dkhrunina.tm.enumerated.Status;
import t1.dkhrunina.tm.model.Project;

public interface IProjectService extends IProjectRepository {

    Project changeProjectStatusById(String id, Status status);

    Project changeProjectStatusByIndex(Integer index, Status status);

    Project updateById(String id, String name, String description);

    Project updateByIndex(Integer index, String name, String description);

}