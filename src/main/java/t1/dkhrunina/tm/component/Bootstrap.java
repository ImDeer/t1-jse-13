package t1.dkhrunina.tm.component;

import t1.dkhrunina.tm.api.controller.ICommandController;
import t1.dkhrunina.tm.api.controller.IProjectController;
import t1.dkhrunina.tm.api.controller.IProjectTaskController;
import t1.dkhrunina.tm.api.controller.ITaskController;
import t1.dkhrunina.tm.api.repository.ICommandRepository;
import t1.dkhrunina.tm.api.repository.IProjectRepository;
import t1.dkhrunina.tm.api.repository.ITaskRepository;
import t1.dkhrunina.tm.api.service.ICommandService;
import t1.dkhrunina.tm.api.service.IProjectService;
import t1.dkhrunina.tm.api.service.IProjectTaskService;
import t1.dkhrunina.tm.api.service.ITaskService;
import t1.dkhrunina.tm.constant.ArgumentConst;
import t1.dkhrunina.tm.constant.CommandConst;
import t1.dkhrunina.tm.controller.CommandController;
import t1.dkhrunina.tm.controller.ProjectController;
import t1.dkhrunina.tm.controller.ProjectTaskController;
import t1.dkhrunina.tm.controller.TaskController;
import t1.dkhrunina.tm.enumerated.Status;
import t1.dkhrunina.tm.model.Project;
import t1.dkhrunina.tm.model.Task;
import t1.dkhrunina.tm.repository.CommandRepository;
import t1.dkhrunina.tm.repository.ProjectRepository;
import t1.dkhrunina.tm.repository.TaskRepository;
import t1.dkhrunina.tm.service.CommandService;
import t1.dkhrunina.tm.service.ProjectService;
import t1.dkhrunina.tm.service.ProjectTaskService;
import t1.dkhrunina.tm.service.TaskService;
import t1.dkhrunina.tm.util.TerminalUtil;

public final class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ITaskController taskController = new TaskController(taskService);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    private final IProjectController projectController = new ProjectController(projectService, projectTaskService);

    private final IProjectTaskController projectTaskController = new ProjectTaskController(projectTaskService);

    private void initDemoData() {
        projectService.add(new Project("Project Baa", Status.IN_PROGRESS));
        projectService.add(new Project("Project Meow", Status.NOT_STARTED));
        projectService.add(new Project("Project Quack", Status.COMPLETED));
        projectService.add(new Project("Project Woof", Status.IN_PROGRESS));

        taskService.add(new Task("Woof-Woof"));
        taskService.add(new Task("Quack-Quack"));
    }

    private void processArguments(final String[] args) {
        if (args == null || args.length < 1) return;
        processArgument(args[0]);
        exit();
    }

    private void processCommands() {
        System.out.println("*** Welcome to Task Manager ***");
        while (!Thread.currentThread().isInterrupted()) {
            System.out.println("\nEnter command: ");
            final String command = TerminalUtil.nextLine();
            processCommand(command);
        }
    }

    private void exit() {
        System.exit(0);
    }

    private void processCommand(final String arg) {
        switch (arg) {
            case CommandConst.VERSION:
                commandController.showVersion();
                break;
            case CommandConst.ABOUT:
                commandController.showAbout();
                break;
            case CommandConst.HELP:
                commandController.showHelp();
                break;
            case CommandConst.INFO:
                commandController.showSystemInfo();
                break;
            case CommandConst.PROJECT_LIST:
                projectController.showProjects();
                break;
            case CommandConst.PROJECT_CHANGE_STATUS_BY_ID:
                projectController.changeProjectStatusById();
                break;
            case CommandConst.PROJECT_CHANGE_STATUS_BY_INDEX:
                projectController.changeProjectStatusByIndex();
                break;
            case CommandConst.PROJECT_CLEAR:
                projectController.clearProjects();
                break;
            case CommandConst.PROJECT_COMPLETE_BY_ID:
                projectController.completeProjectById();
                break;
            case CommandConst.PROJECT_COMPLETE_BY_INDEX:
                projectController.completeProjectByIndex();
                break;
            case CommandConst.PROJECT_CREATE:
                projectController.createProject();
                break;
            case CommandConst.PROJECT_REMOVE_BY_ID:
                projectController.removeProjectById();
                break;
            case CommandConst.PROJECT_REMOVE_BY_INDEX:
                projectController.removeProjectByIndex();
                break;
            case CommandConst.PROJECT_SHOW_BY_ID:
                projectController.showProjectById();
                break;
            case CommandConst.PROJECT_SHOW_BY_INDEX:
                projectController.showProjectByIndex();
                break;
            case CommandConst.PROJECT_START_BY_ID:
                projectController.startProjectById();
                break;
            case CommandConst.PROJECT_START_BY_INDEX:
                projectController.startProjectByIndex();
                break;
            case CommandConst.PROJECT_UPDATE_BY_ID:
                projectController.updateProjectById();
                break;
            case CommandConst.PROJECT_UPDATE_BY_INDEX:
                projectController.updateProjectByIndex();
                break;
            case CommandConst.TASK_LIST:
                taskController.showTasks();
                break;
            case CommandConst.TASK_CHANGE_STATUS_BY_ID:
                taskController.changeTaskStatusById();
                break;
            case CommandConst.TASK_CHANGE_STATUS_BY_INDEX:
                taskController.changeTaskStatusByIndex();
                break;
            case CommandConst.TASK_CLEAR:
                taskController.clearTasks();
                break;
            case CommandConst.TASK_COMPLETE_BY_ID:
                taskController.completeTaskById();
                break;
            case CommandConst.TASK_COMPLETE_BY_INDEX:
                taskController.completeTaskByIndex();
                break;
            case CommandConst.TASK_CREATE:
                taskController.createTask();
                break;
            case CommandConst.TASK_REMOVE_BY_ID:
                taskController.removeTaskById();
                break;
            case CommandConst.TASK_REMOVE_BY_INDEX:
                taskController.removeTaskByIndex();
                break;
            case CommandConst.TASK_SHOW_BY_ID:
                taskController.showTaskById();
                break;
            case CommandConst.TASK_SHOW_BY_INDEX:
                taskController.showTaskByIndex();
                break;
            case CommandConst.TASK_SHOW_BY_PROJECT:
                taskController.showTasksByProjectId();
                break;
            case CommandConst.TASK_START_BY_ID:
                taskController.startTaskById();
                break;
            case CommandConst.TASK_START_BY_INDEX:
                taskController.startTaskByIndex();
                break;
            case CommandConst.TASK_UPDATE_BY_ID:
                taskController.updateTaskById();
                break;
            case CommandConst.TASK_UPDATE_BY_INDEX:
                taskController.updateTaskByIndex();
                break;
            case CommandConst.TASK_BIND_TO_PROJECT:
                projectTaskController.bindTaskToProject();
                break;
            case CommandConst.TASK_UNBIND_FROM_PROJECT:
                projectTaskController.unbindTaskFromProject();
                break;
            case CommandConst.EXIT:
                exit();
                break;
            default:
                commandController.showCommandError();
                break;
        }
    }

    private void processArgument(final String arg) {
        switch (arg) {
            case ArgumentConst.VERSION: {
                commandController.showVersion();
                break;
            }
            case ArgumentConst.ABOUT: {
                commandController.showAbout();
                break;
            }
            case ArgumentConst.HELP: {
                commandController.showHelp();
                break;
            }
            case ArgumentConst.INFO: {
                commandController.showSystemInfo();
                break;
            }
            default: {
                commandController.showArgumentError();
                break;
            }
        }
    }

    public void run(final String... args) {
        processArguments(args);
        initDemoData();
        processCommands();
    }

}