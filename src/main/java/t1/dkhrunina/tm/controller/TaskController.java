package t1.dkhrunina.tm.controller;

import t1.dkhrunina.tm.api.controller.ITaskController;
import t1.dkhrunina.tm.api.service.ITaskService;
import t1.dkhrunina.tm.enumerated.Status;
import t1.dkhrunina.tm.model.Task;
import t1.dkhrunina.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class TaskController implements ITaskController {

    private final ITaskService taskService;

    public TaskController(final ITaskService taskService) {
        this.taskService = taskService;
    }

    private void showTask(final Task task) {
        if (task == null) return;
        System.out.println("Id: " + task.getId());
        System.out.println("Name: " + task.getName());
        if (!task.getDescription().isEmpty()) System.out.println("Description: " + task.getDescription());
        System.out.println("Status: " + Status.toName(task.getStatus()));
        System.out.println("Project id: " + (task.getProjectId() == null ? "not bound" : task.getProjectId()));
    }

    private void renderTasks(final List<Task> tasks) {
        int index = 1;
        for (final Task task : tasks) {
            System.out.println(index + ". " + task.toString());
            index++;
        }
    }

    @Override
    public void changeTaskStatusById() {
        System.out.println("[Change task status by id]");
        System.out.println("Enter id: ");
        final String id = TerminalUtil.nextLine();
        System.out.println("Enter status: ");
        System.out.println(Arrays.toString(Status.toValues()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.toStatus(statusValue);
        final Task task = taskService.changeTaskStatusById(id, status);
        if (task == null) System.out.println("[Error: task not found]");
        else System.out.println("[OK]");
    }

    @Override
    public void changeTaskStatusByIndex() {
        System.out.println("[Change task status by index]");
        System.out.println("Enter index: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("Enter status: ");
        System.out.println(Arrays.toString(Status.toValues()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.toStatus(statusValue);
        final Task task = taskService.changeTaskStatusByIndex(index, status);
        if (task == null) System.out.println("[Error: task not found]");
        else System.out.println("[OK]");
    }

    @Override
    public void clearTasks() {
        System.out.println("[Clear task list]");
        taskService.clear();
        System.out.println("[OK]");
    }

    @Override
    public void completeTaskById() {
        System.out.println("[Complete task by id]");
        System.out.println("Enter id: ");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.changeTaskStatusById(id, Status.COMPLETED);
        if (task == null) System.out.println("[Error: task not found]");
        else System.out.println("[OK]");
    }

    @Override
    public void completeTaskByIndex() {
        System.out.println("[Complete task by index]");
        System.out.println("Enter index: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.changeTaskStatusByIndex(index, Status.COMPLETED);
        if (task == null) System.out.println("[Error: task not found]");
        else System.out.println("[OK]");
    }

    @Override
    public void createTask() {
        System.out.println("[Create task]");
        System.out.println("Enter name: ");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter description: ");
        final String description = TerminalUtil.nextLine();
        final Task task = taskService.create(name, description);
        if (task == null) System.out.println("[Failed to create a task]");
        else System.out.println("[OK]");
    }

    @Override
    public void removeTaskById() {
        System.out.println("[Remove task by id]");
        System.out.println("Enter id: ");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.removeById(id);
        if (task == null) System.out.println("[Error: task not found]");
        else System.out.println("[OK]");
    }

    @Override
    public void removeTaskByIndex() {
        System.out.println("[Remove task by index]");
        System.out.println("Enter index: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.removeByIndex(index);
        if (task == null) System.out.println("[Error: task not found]");
        else System.out.println("[OK]");
    }

    @Override
    public void showTaskById() {
        System.out.println("[Show task by id]");
        System.out.println("Enter id: ");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.findOneById(id);
        if (task == null) {
            System.out.println("[Error: task not found]");
            return;
        }
        showTask(task);
        System.out.println("[OK]");
    }

    @Override
    public void showTaskByIndex() {
        System.out.println("[Show task by index]");
        System.out.println("Enter index: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.findOneByIndex(index);
        if (task == null) {
            System.out.println("[Error: task not found]");
            return;
        }
        showTask(task);
        System.out.println("[OK]");
    }

    @Override
    public void showTasks() {
        System.out.println("[Show tasks]");
        final List<Task> tasks = taskService.findAll();
        renderTasks(tasks);
        System.out.println("[OK]");
    }

    @Override
    public void showTasksByProjectId() {
        System.out.println("[Task list by project id]");
        System.out.println("Enter project id: ");
        final String projectId = TerminalUtil.nextLine();
        final List<Task> tasks = taskService.findAllByProjectId(projectId);
        renderTasks(tasks);
        System.out.println("[OK]");
    }

    @Override
    public void startTaskById() {
        System.out.println("[Start task by id]");
        System.out.println("Enter id: ");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.changeTaskStatusById(id, Status.IN_PROGRESS);
        if (task == null) System.out.println("[Error: task not found]");
        else System.out.println("[OK]");
    }

    @Override
    public void startTaskByIndex() {
        System.out.println("[Start task by index]");
        System.out.println("Enter index: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.changeTaskStatusByIndex(index, Status.IN_PROGRESS);
        if (task == null) System.out.println("[Error: task not found]");
        else System.out.println("[OK]");
    }

    @Override
    public void updateTaskById() {
        System.out.println("[Update task by id]");
        System.out.println("Enter id: ");
        final String id = TerminalUtil.nextLine();
        if (taskService.findOneById(id) == null) {
            System.out.println("[Error: task not found]");
            return;
        }
        System.out.println("Enter name: ");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter description (optional): ");
        final String description = TerminalUtil.nextLine();
        final Task task = taskService.updateById(id, name, description);
        if (task == null) System.out.println("[Fail]");
        else System.out.println("[OK]");
    }

    @Override
    public void updateTaskByIndex() {
        System.out.println("[Update task by index]");
        System.out.println("Enter index: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        if (taskService.findOneByIndex(index) == null) {
            System.out.println("[Error: task not found]");
            return;
        }
        System.out.println("Enter name: ");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter description (optional): ");
        final String description = TerminalUtil.nextLine();
        final Task task = taskService.updateByIndex(index, name, description);
        if (task == null) System.out.println("[Fail]");
        else System.out.println("[OK]");
    }

}