package t1.dkhrunina.tm.controller;

import t1.dkhrunina.tm.api.controller.IProjectTaskController;
import t1.dkhrunina.tm.api.service.IProjectTaskService;
import t1.dkhrunina.tm.model.Task;
import t1.dkhrunina.tm.util.TerminalUtil;

public class ProjectTaskController implements IProjectTaskController {

    private final IProjectTaskService projectTaskService;

    public ProjectTaskController(final IProjectTaskService projectTaskService) {
        this.projectTaskService = projectTaskService;
    }

    @Override
    public void bindTaskToProject() {
        System.out.println("[Bind task to project]");
        System.out.println("Enter project id: ");
        final String projectId = TerminalUtil.nextLine();
        System.out.println("Enter task id: ");
        final String taskId = TerminalUtil.nextLine();
        final Task task = projectTaskService.bindTaskToProject(projectId, taskId);
        if (task == null) System.out.println("[Error]");
        else System.out.println("[OK]");
    }

    @Override
    public void unbindTaskFromProject() {
        System.out.println("[Unbind task from project]");
        System.out.println("Enter project id: ");
        final String projectId = TerminalUtil.nextLine();
        System.out.println("Enter task id: ");
        final String taskId = TerminalUtil.nextLine();
        final Task task = projectTaskService.unbindTaskFromProject(projectId, taskId);
        if (task == null) System.out.println("[Error]");
        else System.out.println("[OK]");
    }

}