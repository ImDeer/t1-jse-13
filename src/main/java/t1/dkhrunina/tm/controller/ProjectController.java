package t1.dkhrunina.tm.controller;

import t1.dkhrunina.tm.api.controller.IProjectController;
import t1.dkhrunina.tm.api.service.IProjectService;
import t1.dkhrunina.tm.api.service.IProjectTaskService;
import t1.dkhrunina.tm.enumerated.Status;
import t1.dkhrunina.tm.model.Project;
import t1.dkhrunina.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class ProjectController implements IProjectController {

    private final IProjectService projectService;

    private final IProjectTaskService projectTaskService;

    public ProjectController(
            final IProjectService projectService,
            final IProjectTaskService projectTaskService
    ) {
        this.projectService = projectService;
        this.projectTaskService = projectTaskService;
    }

    private void showProject(final Project project) {
        if (project == null) return;
        System.out.println("Id: " + project.getId());
        System.out.println("Name: " + project.getName());
        if (!project.getDescription().isEmpty()) System.out.println("Description: " + project.getDescription());
        System.out.println("Status: " + Status.toName(project.getStatus()));
    }

    @Override
    public void changeProjectStatusById() {
        System.out.println("[Change project status by id]");
        System.out.println("Enter id: ");
        final String id = TerminalUtil.nextLine();
        System.out.println("Enter status: ");
        System.out.println(Arrays.toString(Status.toValues()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.toStatus(statusValue);
        final Project project = projectService.changeProjectStatusById(id, status);
        if (project == null) System.out.println("[Error: project not found]");
        else System.out.println("[OK]");
    }

    @Override
    public void changeProjectStatusByIndex() {
        System.out.println("[Change project status by index]");
        System.out.println("Enter index: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("Enter status: ");
        System.out.println(Arrays.toString(Status.toValues()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.toStatus(statusValue);
        final Project project = projectService.changeProjectStatusByIndex(index, status);
        if (project == null) System.out.println("[Error: project not found]");
        else System.out.println("[OK]");
    }

    @Override
    public void clearProjects() {
        System.out.println("[Clear project list]");
        projectService.clear();
        System.out.println("[OK]");
    }

    @Override
    public void completeProjectById() {
        System.out.println("[Complete project by id]");
        System.out.println("Enter id: ");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.changeProjectStatusById(id, Status.COMPLETED);
        if (project == null) System.out.println("[Error: project not found]");
        else System.out.println("[OK]");
    }

    @Override
    public void completeProjectByIndex() {
        System.out.println("[Complete project by index]");
        System.out.println("Enter index: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.changeProjectStatusByIndex(index, Status.COMPLETED);
        if (project == null) System.out.println("[Error: project not found]");
        else System.out.println("[OK]");
    }

    @Override
    public void createProject() {
        System.out.println("[Create project]");
        System.out.println("Enter name: ");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter description: ");
        final String description = TerminalUtil.nextLine();
        final Project project = projectService.create(name, description);
        if (project == null) System.out.println("[Failed to create a project]");
        else System.out.println("[OK]");
    }

    @Override
    public void removeProjectById() {
        System.out.println("[Remove project by id]");
        System.out.println("Enter id: ");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.findOneById(id);
        if (project == null) {
            System.out.println("[Error: project not found]");
            return;
        }
        projectTaskService.removeProjectById(id);
        System.out.println("[OK]");
    }

    @Override
    public void removeProjectByIndex() {
        System.out.println("[Remove project by index]");
        System.out.println("Enter index: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.findOneByIndex(index);
        if (project == null) {
            System.out.println("[Error: project not found]");
            return;
        }
        projectTaskService.removeProjectById(project.getId());
        System.out.println("[OK]");
    }

    @Override
    public void showProjectById() {
        System.out.println("[Show project by id]");
        System.out.println("Enter id: ");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.findOneById(id);
        if (project == null) {
            System.out.println("[Error: project not found]");
            return;
        }
        showProject(project);
        System.out.println("[OK]");
    }

    @Override
    public void showProjectByIndex() {
        System.out.println("[Show project by index]");
        System.out.println("Enter index: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.findOneByIndex(index);
        if (project == null) {
            System.out.println("[Error: project not found]");
            return;
        }
        showProject(project);
        System.out.println("[OK]");
    }

    @Override
    public void showProjects() {
        System.out.println("[Project list]");
        final List<Project> projects = projectService.findAll();
        int index = 1;
        for (final Project project : projects) {
            System.out.println(index + ". " + project.toString());
            index++;
        }
        System.out.println("[OK]");
    }

    @Override
    public void startProjectById() {
        System.out.println("[Start project by id]");
        System.out.println("Enter id: ");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.changeProjectStatusById(id, Status.IN_PROGRESS);
        if (project == null) System.out.println("[Error: project not found]");
        else System.out.println("[OK]");
    }

    @Override
    public void startProjectByIndex() {
        System.out.println("[Start project by index]");
        System.out.println("Enter index: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.changeProjectStatusByIndex(index, Status.IN_PROGRESS);
        if (project == null) System.out.println("[Error: project not found]");
        else System.out.println("[OK]");
    }

    @Override
    public void updateProjectById() {
        System.out.println("[Update project by id]");
        System.out.println("Enter id: ");
        final String id = TerminalUtil.nextLine();
        if (projectService.findOneById(id) == null) {
            System.out.println("[Error: project not found]");
            return;
        }
        System.out.println("Enter name: ");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter description (optional): ");
        final String description = TerminalUtil.nextLine();
        final Project project = projectService.updateById(id, name, description);
        if (project == null) System.out.println("[Fail]");
        else System.out.println("[OK]");
    }

    @Override
    public void updateProjectByIndex() {
        System.out.println("[Update project by index]");
        System.out.println("Enter index: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        if (projectService.findOneByIndex(index) == null) {
            System.out.println("[Error: project not found]");
            return;
        }
        System.out.println("Enter name: ");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter description (optional): ");
        final String description = TerminalUtil.nextLine();
        final Project project = projectService.updateByIndex(index, name, description);
        if (project == null) System.out.println("[Fail]");
        else System.out.println("[OK]");
    }

}